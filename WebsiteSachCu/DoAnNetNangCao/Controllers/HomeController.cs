﻿using DoAnNetNangCao.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Linq;
using WebSachCu.Models;
using Microsoft.AspNetCore.Session;
namespace DoAnNetNangCao.Controllers
{
    public class HomeController : Controller
    {
        WebsiteSachCuContext db = new WebsiteSachCuContext();
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {        
            User user = db.Users.SingleOrDefault(x => x.UserName == username && x.Password == password);
            Customer cus = db.Customers.SingleOrDefault(x => x.UserName == username && x.Password == password);
            if (user != null)
            {
                HttpContext.Session.SetInt32("Userid", user.UsersId);
                HttpContext.Session.SetString("fullname", user.FullName);
                HttpContext.Session.SetString("username", user.UserName);
                HttpContext.Session.SetString("avatar", user.Avatar);
                //HttpContext.Session<Customer>("Customer" , cus);
                return Redirect("/Shop/Index");

            }
            
            if(cus != null )
            {
                HttpContext.Session.SetInt32("customerid", cus.CustomerId);
                HttpContext.Session.SetString("username", cus.UserName);
                HttpContext.Session.SetString("customername", cus.CustomerName);
                HttpContext.Session.SetString("avatar", cus.Avatar);
                HttpContext.Session.Set<Customer>("customer", cus);
                return Redirect("/Shop/Index");
            }

            ViewBag.error = "<p style='color:red;'>Sai tên đăng nhập hoặc mật khẩu!</p>";
            return View(user);


        }

        public IActionResult Logout()
        {

            HttpContext.Session.Clear();
            return Redirect("/Shop/Index");

        }



    }
}
