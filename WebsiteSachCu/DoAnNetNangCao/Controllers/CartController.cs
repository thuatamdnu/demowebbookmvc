﻿using DoAnNetNangCao.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;

namespace DoAnNetNangCao.Controllers
{
    public class CartController : Controller
    {
        WebsiteSachCuContext db = new WebsiteSachCuContext();
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddToCart(string id)
        {
            List<Cartitem> carts = null;
            if (HttpContext.Session.Get<List<Cartitem>>("Cart") == null)//nếu giỏ hàng rỗng
            {
                carts = new List<Cartitem>();
                carts.Add(new Cartitem { BookOrder = db.Books.Find(id), Quantity = 1 });
            }
            else// đã có sp trong giỏ hàng
            {
                carts = HttpContext.Session.Get<List<Cartitem>>("Cart") as List<Cartitem>;
                Cartitem product = carts.SingleOrDefault(x => x.BookOrder.BookId == id);
                if (product != null)// đã có sản phẩm trong giỏ hàng
                {
                    product.Quantity++;
                }
                else
                {
                    carts.Add(new Cartitem { BookOrder = db.Books.Find(id), Quantity = 1 });
                }
            }
            //Cập nhật lại session
            HttpContext.Session.Set<List<Cartitem>>("Cart", carts);
            //trả về tổng số lượng hàng hóa cần mua
            return Json(new { total = carts.Sum(x => x.Quantity) });

        }

        public IActionResult ShoppingCart()
        {

            return View();
        }
        [HttpGet]
        public IActionResult Order()
        {
            //kiểm tra khách hàng đã đăng nhập
            if(HttpContext.Session.Get<Customer>("Customer") == null)
            {
                return RedirectToAction("Login", "Home");
            }
            // kiểm tra giỏ hàng
            if(HttpContext.Session.Get<List<Cartitem>>("Cart") == null)
            {
                return RedirectToAction("Index", "Shop");
            }
            // lấy dữ liệu từ Session
            List<Cartitem> carts = (List<Cartitem>)HttpContext.Session.Get<List<Cartitem>>("cart");
            return View(carts);

        }
        [HttpPost]
        public IActionResult CreateOrder()
        {
            // lưu đơn đặt hàn
            Order order = new Order();
            Cartitem cart = new Cartitem();
            
            Customer customer = (Customer) HttpContext.Session.Get<Customer>("customer");
            List<OrdDetailsItem> ordDetails = (List < OrdDetailsItem >) HttpContext.Session.Get<List<OrdDetailsItem>>("orderdetails");

            List<Cartitem> carts = (List<Cartitem>) HttpContext.Session.Get<List<Cartitem>>("Cart");
            order.OrderId = new Random().Next(); // sinh ra số ngẫu nhiên bất kì
            order.CustomerId = customer.CustomerId;
            double sum = 0;
            
            foreach (var item in carts)
            {
                string giamoi = (string)item.BookOrder.NewPrice;
                double dongia = Convert.ToDouble(giamoi);
                sum += item.Quantity * dongia;
            }
            order.TotalMoney = sum;
            order.Payment = "thanh toán khi nhận hàng";
            order.DateOrder = DateTime.Now;
            db.Orders.Add(order);
            db.SaveChanges();
            //lưu chi tiết đơn đặt hàng
            
            foreach (var item in carts)
            {
                OrderDetail od = new OrderDetail();
                od.OrderDetailId = new Random().Next();
                od.BookId = item.BookOrder.BookId;
                od.OrderId = order.OrderId;
                od.Quantity = item.Quantity;
                db.OrderDetails.Add(od);
                db.SaveChanges();
            }

            
            HttpContext.Session.Set<List<Cartitem>>("Cart", null);

            return Redirect("/Cart/OrderConfirm");
        }

        public IActionResult OrderConfirm()
        {
            return View();
        }
    }
}
