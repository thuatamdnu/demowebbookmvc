﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;
using Newtonsoft.Json.Serialization;

namespace DoAnNetNangCao.Controllers
{
    public class ShopController : Controller
    {
        WebsiteSachCuContext db = new WebsiteSachCuContext();
        public IActionResult Index()
        {
            
            return View(db.Books.ToList());
        }
        public IActionResult ListNew()
        {
            var listNew = db.Books.OrderByDescending(x => x.BookId).Take(10).ToList();
            return View(listNew);
           
        }      
        
        public IActionResult ListBest()
        {
            var listBest = db.Books.OrderByDescending(x => x.Sale).Take(10).ToList();

            return View(listBest);
        }
        public JsonResult Same()
        {
            var listNew = db.Books.OrderByDescending(x => x.Category).Take(5).ToList();
            return Json(listNew);

        }
        

        public IActionResult Detail(string id)
        {
            //// lấy thông tin id chuyền vào
            //var objProduct = db.Books.Where(x => x.BookId == id).FirstOrDefault();


            //ProductDetail objRroductDetail = new ProductDetail();
            //objRroductDetail.objBook = objProduct;
            //return View(objRroductDetail);
            Book c = db.Books.SingleOrDefault(x => x.BookId == id);

            return View(c);


        }

        public IActionResult NewIndex()
        {
            var listNew = db.Books.OrderByDescending(x => x.BookId).Take(6).ToList();
            var listBest = db.Books.OrderByDescending(x => x.Sale).Take(6).ToList();
            var tuple = new Tuple<List<Book>, List<Book>>(listNew, listBest);
            return View(tuple);
        }
        
    }
}
