﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;

namespace DoAnNetNangCao.Controllers
{
    public class ProductController : Controller
    {

        WebsiteSachCuContext db = new WebsiteSachCuContext();
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Details(int id)
        {
            Book b = db.Books.Find(id);
            return View(b);            
        }
        public IActionResult SreachByName(string name)
        {
            @ViewBag.keyword = name;
            List<Book> pList = db.Books.Where(x => x.BookName.Contains(name)).ToList();
            return View(pList);
        }
    }
}
