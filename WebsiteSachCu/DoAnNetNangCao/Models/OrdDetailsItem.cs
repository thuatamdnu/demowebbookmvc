﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;

namespace DoAnNetNangCao.Models
{
    public class OrdDetailsItem
    {
        public OrderDetail  Odd { get; set; }
        public Book BookDetails { get; set; }
    }
}
