﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;

namespace DoAnNetNangCao.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CusController : Controller
    {
        WebsiteSachCuContext db = new WebsiteSachCuContext();
        public IActionResult Index()
        {
            return View(db.Customers.ToList());
        }
    }
}
