﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;
using DoAnNetNangCao.Models;

namespace DoAnNetNangCao.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomeController : Controller
    {
        WebsiteSachCuContext db = new WebsiteSachCuContext();
        ProcessingLogic logic = new ProcessingLogic();
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [Area("Admin")]
        [HttpPost]
        public IActionResult Login(string username, string password)
        {
          
            User user = db.Users.SingleOrDefault(x => x.UserName == username && x.Password == password);
            if (user != null)
            {
                HttpContext.Session.SetInt32("Userid", user.UsersId);
                HttpContext.Session.SetString("fullname", user.FullName);
                HttpContext.Session.SetString("username", user.UserName);
                HttpContext.Session.SetString("avatar", user.Avatar);
                return Redirect("/Admin/Home/Index");   
                
            }
            ViewBag.error = "<p style='color:red;'>Sai tên đăng nhập hoặc mật khẩu!</p>";
            return View(user);

        }
        [Area("Admin")]
        [HttpPost]
        public IActionResult LoginShop(string username, string password)
        {

            User user = db.Users.SingleOrDefault(x => x.UserName == username && x.Password == password);
            if (user != null)
            {
                HttpContext.Session.SetInt32("Userid", user.UsersId);
                HttpContext.Session.SetString("fullname", user.FullName);
                HttpContext.Session.SetString("username", user.UserName);
                HttpContext.Session.SetString("avatar", user.Avatar);
                return Redirect("/Shop/Index");

            }
            ViewBag.error = "<p style='color:red;'>Sai tên đăng nhập hoặc mật khẩu!</p>";
            return View(user);

        }
    }
}
