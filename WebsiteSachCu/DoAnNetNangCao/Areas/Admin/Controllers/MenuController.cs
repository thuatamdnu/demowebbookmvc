﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;

namespace DoAnNetNangCao.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class MenuController : Controller
    {
        WebsiteSachCuContext db = new WebsiteSachCuContext();
        public IActionResult Index()
        {
            return View(db.Menus.ToList());
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Menu m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Menus.Add(m);
                    db.SaveChanges();
                    return Redirect("/Admin/Menu/Index");
                }
            }
            catch (Exception ex)
            {

                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(m);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            Menu c = db.Menus.SingleOrDefault(x => x.MenuId == id);
            return View(c);
        }
        [HttpPost]
        public IActionResult Edit(Menu m)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(m).State = EntityState.Modified;
                    db.SaveChanges();
                    return Redirect("/Admin/Menu/Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(m);
        }

        public IActionResult Delete(int id)
        {
            try
            {
                Menu m = db.Menus.SingleOrDefault(x => x.MenuId == id);
                db.Menus.Remove(m);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return Redirect("/Admin/Menu/Index");
        }
    }
}
