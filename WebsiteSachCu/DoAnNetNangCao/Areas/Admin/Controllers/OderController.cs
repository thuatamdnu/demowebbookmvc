﻿ using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;
using Newtonsoft.Json.Serialization;

namespace DoAnNetNangCao.Areas.Admin
{
    [Area("Admin")]
    public class OderController : Controller
    {
        WebsiteSachCuContext db = new WebsiteSachCuContext();
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            return Json(db.Orders.ToList());
        }
        [HttpPost]
        public JsonResult Create([FromBody] Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
            return Json(order);
        }
        public JsonResult GetById(int id) {
            Order od = db.Orders.SingleOrDefault(x => x.OrderId == id);
            return Json(od);
        }
        [HttpPost]
        public JsonResult Update([FromBody] Order od)
        {
            db.Entry(od).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
            return Json(od);
        }
        public JsonResult Delete(int id)
        {
            Order od = db.Orders.SingleOrDefault(x => x.OrderId == id);
            db.Orders.Remove(od);
            db.SaveChanges();
            return Json(od);
        }
    }
}
