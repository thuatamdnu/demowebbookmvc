﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;

namespace DoAnNetNangCao.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class BookController : Controller
    {
        WebsiteSachCuContext db = new WebsiteSachCuContext();
        public IActionResult Index()
        {                         
           return View(db.Books.ToList());
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Book b)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Books.Add(b);
                    db.SaveChanges();
                    return Redirect("/Admin/Book/Index");
                }
            }
            catch (Exception ex)
            {

                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(b);
        }
        [HttpGet]
        public IActionResult Edit(string id)
        {
            Book c = db.Books.SingleOrDefault(x => x.BookId == id);
            return View(c);
        }
        [HttpPost]
        public IActionResult Edit(Book  b)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(b).State = EntityState.Modified;
                    db.SaveChanges();
                    return Redirect("/Admin/Book/Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(b);
        }

        public IActionResult Delete(string id)
        {
            try
            {
                Book  c = db.Books.SingleOrDefault(x => x.BookId == id);
                db.Books.Remove(c);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
            }
            return Redirect("/Admin/Book/Index");
        }
    }
}
