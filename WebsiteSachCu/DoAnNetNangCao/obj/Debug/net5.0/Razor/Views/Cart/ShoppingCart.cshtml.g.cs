#pragma checksum "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "26f7f8acacb08985b0ba4805071d2d51cf831d0e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Cart_ShoppingCart), @"mvc.1.0.view", @"/Views/Cart/ShoppingCart.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\_ViewImports.cshtml"
using DoAnNetNangCao;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\_ViewImports.cshtml"
using DoAnNetNangCao.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
using WebSachCu.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"26f7f8acacb08985b0ba4805071d2d51cf831d0e", @"/Views/Cart/ShoppingCart.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"92987d7ac6143d8fc24a8b87ecc0bd693e81e842", @"/Views/_ViewImports.cshtml")]
    public class Views_Cart_ShoppingCart : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("/Cart/CreateOrder"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("form_Login"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
  
    ViewData["Title"] = "ShoppingCart";
    Layout = "~/Views/Shared/_LayoutShop.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<style>
    .model{
        display: none;
    }
</style>
<script>
    $('.add').click(function () {
        if ($(this).prev().val() < 3) {
            $(this).prev().val(+$(this).prev().val() + 1);
        }
    });
    $('.sub').click(function () {
        if ($(this).next().val() > 1) {
            if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
        }
    });
    function openModaOrder() {
        document.getElementById(""modalOrder"").style.display = ""flex"";
    }

    function closeModalOrder() {
        document.getElementById(""modalOrder"").style.display = ""none"";
    }
    function openLoginOrder() {
        document.getElementById(""orderUpdate"").style.display = ""block"";
        document.getElementById(""orderForm"").style.display = ""none"";
    }
    function openRegisterOrder() {
        document.getElementById(""orderForm"").style.display = ""block"";
        document.getElementById(""orderUpdate"").style.display = ""none"";
    }
</script>
");
#nullable restore
#line 38 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
  
    double tongsp = 0;
    double tongtien = 0;
    List<Cartitem> carts = (List<Cartitem>)Context.Session.Get<List<Cartitem>>("Cart");


#line default
#line hidden
#nullable disable
            WriteLiteral(@"
    <div class=""Product  grid wide"">
        <div class=""MainOrder"">
            <div class=""MainOrder_header row"">
                <div class=""MainOrder_header-product m-6"">Sản phẩm</div>
                <div class=""MainOrder_header-price m-2"">Đơn Giá</div>
                <div class=""MainOrder_header-price m-2"">Số lượng</div>
                <div class=""MainOrder_header-price m-2"">Số tiền</div>
            </div>
");
#nullable restore
#line 53 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
             if (carts != null)
            {
                foreach (var item in carts)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <div class=\"MainOrder_Product row\">\r\n                        <div class=\"MainOrder_Product-product m-6 \">\r\n                            <img class=\"MainOrder_Product--img\"");
            BeginWriteAttribute("src", " src=\"", 2022, "\"", 2060, 2);
            WriteAttributeValue("", 2028, "/Book/img/", 2028, 10, true);
#nullable restore
#line 59 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
WriteAttributeValue("", 2038, item.BookOrder.Images, 2038, 22, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("alt", " alt=\"", 2061, "\"", 2067, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n                            <div class=\"MainOrder_Product--name m-6\">");
#nullable restore
#line 60 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                                                                Write(item.BookOrder.BookName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n                        </div>\r\n                        <div class=\"MainOrder_Product--price m-2\">");
#nullable restore
#line 62 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                                                             Write(item.BookOrder.NewPrice);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</div>
                        <div class=""MainOrder_Product--quanity m-2 number"">

                            <input type=""number"" id=""1"" value=""1"" min=""1"" max=""3"" />

                        </div>
                        <div class=""MainOrder_Product--price m-2"">
");
#nullable restore
#line 69 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                              
                                string giamoi = (string)item.BookOrder.NewPrice;
                                double gia = Convert.ToDouble(giamoi);
                                double tong = gia * item.Quantity;
                                tongtien += tong;
                                tongsp += item.Quantity;

                            

#line default
#line hidden
#nullable disable
            WriteLiteral("                            ");
#nullable restore
#line 77 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                       Write(tong);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n");
#nullable restore
#line 80 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                }
            }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"


        </div>
        <div class=""MainOrder_Product footer-voucher row"">
            <div class=""m-6""></div>
            <div class=""Voucher m-3""><i class=""fa fa-ticket"" aria-hidden=""true""></i>Voucher</div>
            <div class=""Choose_Voucher m-3"">Chọn Voucher</div>
        </div>
        <div class=""MainOrder_Product row"">
            <div class=""m-5"">");
#nullable restore
#line 92 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                        Write(DateTime.Now.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n            <div class=\"pay m-4\">Tổng thanh toán(");
#nullable restore
#line 93 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                                            Write(tongsp);

#line default
#line hidden
#nullable disable
            WriteLiteral(" sản phẩm): <p class=\"price-pay\">");
#nullable restore
#line 93 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                                                                                    Write(tongtien);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p></div>\r\n            <div class=\"pay-button m-3\"><button class=\"button-pay\" onclick=\"openModaOrder()\">Thanh toán</button></div>\r\n        </div>\r\n    </div>\r\n\r\n");
#nullable restore
#line 98 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
  
Customer cus = (Customer)Context.Session.Get<Customer>("customer");

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"modal\" id=\"modalOrder\" >\r\n    <div class=\"modal__overlay\">\r\n\r\n    </div>\r\n    <div class=\"modal__body\">\r\n        <div class=\"modal__inner\">\r\n            <!-- register form -->\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "26f7f8acacb08985b0ba4805071d2d51cf831d0e12597", async() => {
                WriteLiteral(@"
                <div class=""auth-form"" id=""orderForm"">
                    <div class=""auth-form__container"">
                        <div class=""auth-form_header"">
                            <h3 class=""auth-form__heading"">Thông tin khách hàng</h3>
                            <span class=""auth-form--switch-btn"" onclick=""openLoginOrder()"">cập nhận thông tin</span>
                        </div>

                        <div class=""auth-form__form"">
                            <div class=""auth-form__group"">
                                <p class=""auth-form__group-infor"">Họ Tên: ");
#nullable restore
#line 119 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                                                                     Write(cus.CustomerName);

#line default
#line hidden
#nullable disable
                WriteLiteral("</p>\r\n                            </div>\r\n                            <div class=\"auth-form__group\">\r\n                                <p class=\"auth-form__group-infor\">Địa chỉ:");
#nullable restore
#line 122 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                                                                     Write(cus.Address);

#line default
#line hidden
#nullable disable
                WriteLiteral(" </p>\r\n                            </div>\r\n                            <div class=\"auth-form__group\">\r\n                                <p class=\"auth-form__group-infor\">Điện thoại:");
#nullable restore
#line 125 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                                                                        Write(cus.Phone);

#line default
#line hidden
#nullable disable
                WriteLiteral("</p>\r\n                            </div>\r\n                            <div class=\"auth-form__group\">\r\n                                <p class=\"auth-form__group-infor\">Ngày đặt:");
#nullable restore
#line 128 "E:\DoAnNam2,3\TH LTNET nang cao\WebsiteSachCu\DoAnNetNangCao\Views\Cart\ShoppingCart.cshtml"
                                                                      Write(DateTime.Now.ToShortDateString());

#line default
#line hidden
#nullable disable
                WriteLiteral(@"</p>
                            </div>

                            <div class=""auth-form__controls"">
                                <button class=""btn auth-form__controls-back btn--normal"" onclick=""closeModalOrder()"">TRỞ LẠI</button>
                                <button class=""btn btn--primary"" onclick=""location.href=''"">Đồng ý Đặt Hàng</button>
                            </div>
                            <div class=""auth-form__socials"">
                                <a");
                BeginWriteAttribute("href", " href=\"", 5787, "\"", 5794, 0);
                EndWriteAttribute();
                WriteLiteral(@" class=""auth-form__controls--fb btn btn--size-s btn--with-icon"">
                                    <i class=""auth-form__socials-icon fab fa-facebook""></i>
                                    <span class=""auth-form__controls-label"">Kết nối với FaceBook </span>
                                </a>
                                <a");
                BeginWriteAttribute("href", " href=\"", 6132, "\"", 6139, 0);
                EndWriteAttribute();
                WriteLiteral(@" class=""auth-form__controls--gg btn btn--size-s btn--with-icon"">
                                    <i class=""auth-form__socials-icon fab fa-google""></i>
                                    <span class=""auth-form__controls-label"">Kết nối với Google </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
            
            <div class=""auth-form update-Address"" style=""display: none;"" id=""orderUpdate"">
                <div class=""auth-form__container"">
                    <div class=""auth-form_header"">
                        <h3 class=""auth-form__heading"">Nhập địa chỉ mới</h3>
                        <span class=""auth-form--switch-btn"" onclick=""openRegisterOrder()"">Địa chỉ</span>
                    </div>

                    <div class=""auth-form__form loginjson"">

                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "26f7f8acacb08985b0ba4805071d2d51cf831d0e18632", async() => {
                WriteLiteral(@"


                            <div class=""auth-form__group"">

                                <input type=""text"" class=""auth-form__input"" placeholder=""Nhập Địa chỉ mới"" required=""required"" >
                            </div>
                            <div class=""auth-form__group"">
                                <input type=""password"" class=""auth-form__input"" placeholder=""Nhập Số điện thoại"" required=""required"" >
                            </div>


                            <div class=""auth-form__aside"">
                                <div class=""auth-form--help"">
                                    <a");
                BeginWriteAttribute("href", " href=\"", 7780, "\"", 7787, 0);
                EndWriteAttribute();
                WriteLiteral(" class=\"auth-form__help-link -\"></a>\r\n                                    <span class=\"auth-form__help-sp\"></span>\r\n                                    <a");
                BeginWriteAttribute("href", " href=\"", 7942, "\"", 7949, 0);
                EndWriteAttribute();
                WriteLiteral(@" class=""auth-form__help-link""></a>
                                </div>

                            </div>
                            <div class=""auth-form__controls"">
                                <button class=""btn auth-form__controls-back btn--normal"" onclick=""closeModal()"">TRỞ LẠI</button>
                                <button type=""submit"" class=""btn btn--primary"">Cập nhật</button>
                            </div>

                        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n                        <div class=\"auth-form__socials\">\r\n\r\n                            <a");
            BeginWriteAttribute("href", " href=\"", 8526, "\"", 8533, 0);
            EndWriteAttribute();
            WriteLiteral(@" class=""auth-form__controls--fb btn btn--size-s btn--with-icon"">
                                <i class=""auth-form__socials-icon fab fa-facebook""></i>
                                <span class=""auth-form__controls-label"">Kết nối với FaceBook </span>
                            </a>
                            <a");
            BeginWriteAttribute("href", " href=\"", 8855, "\"", 8862, 0);
            EndWriteAttribute();
            WriteLiteral(@" class=""auth-form__controls--gg btn btn--size-s btn--with-icon"">
                                <i class=""auth-form__socials-icon fab fa-google""></i>
                                <span class=""auth-form__controls-label"">Kết nối với Google </span>
                            </a>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
