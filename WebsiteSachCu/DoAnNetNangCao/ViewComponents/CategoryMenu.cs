﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebSachCu.Models;

namespace DoAnNetNangCao.ViewComponents
{
    public class CategoryMenu :ViewComponent
    {
        WebsiteSachCuContext db = new WebsiteSachCuContext();
        public IViewComponentResult Invoke()
        {
            return View(db.Menus);
        }
    }
}
