USE master
GO
CREATE DATABASE WebsiteSachCu
GO
USE WebsiteSachCu
GO

create table Customers
(
	CustomerID int primary key,
	CustomerName nvarchar(50),
	Gender bit,
	[Address] nvarchar(MAX),
	Email varchar(100),
	Phone varchar(12),
	UserName varchar(50),
	[Password] varchar(50),
	Avatar varchar(MAX)
)
create table BOOKS
(
	BookID  nvarchar(50) primary key ,
	BookName nvarchar(100) ,
	OldPrice nvarchar(100) ,
	NewPrice nvarchar(100),
	Sale nvarchar(50) ,
	Images nvarchar(max) ,
	Description nvarchar(max) ,
	Category nvarchar(50),
	Author nvarchar(50)
)



create table Orders
(
	OrderID int primary key,
	CustomerID int foreign key references Customers(CustomerID),
	TotalMoney float,
	Payment nvarchar(50),
	DateOrder datetime,
	Note nvarchar(MAX)
)
create table OrderDetail
(
	OrderDetailID int primary key,
	BookID NVARCHAR(50) foreign key references BOOKS(BookID),
	OrderID int foreign key references Orders(OrderID),
	Quantity int
)
create table Users
(
	UsersID int primary key,
	UserName varchar(50),
	[Password] varchar(50),
	FullName nvarchar(200),
	Avatar varchar(MAX),
	Email varchar(50),
	Active bit
)
create table Menus
(
	MenuID int primary key,
	Name nvarchar(50),
	Link varchar(MAX),
	Orders int,
	ParentID int
)
create table Sliders
(
	SliderID int primary key,
	Images varchar(MAX),
	Link varchar(MAX)
)