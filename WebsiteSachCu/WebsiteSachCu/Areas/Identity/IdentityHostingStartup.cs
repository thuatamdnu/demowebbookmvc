﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebsiteSachCu.Areas.Identity.Data;
using WebsiteSachCu.Data;

[assembly: HostingStartup(typeof(WebsiteSachCu.Areas.Identity.IdentityHostingStartup))]
namespace WebsiteSachCu.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<WebsiteSachCuDbContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("WebsiteSachCuDbContextConnection")));

                services.AddDefaultIdentity<WebsiteSachCuUser>(options => {
                    options.SignIn.RequireConfirmedAccount = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                    })
                    .AddEntityFrameworkStores<WebsiteSachCuDbContext>();
            });
        }
    }
}