﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebsiteSachCu.Models;

namespace WebsiteSachCu.Data
{
    public class WebsiteSachCuContext : DbContext
    {
        public WebsiteSachCuContext (DbContextOptions<WebsiteSachCuContext> options)
            : base(options)
        {
        }

        public DbSet<WebsiteSachCu.Models.BookViewModel> BookViewModel { get; set; }
    }
}
