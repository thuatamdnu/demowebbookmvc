﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebSachCu.Models
{
    public partial class Book
    {
        public Book()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public string BookId { get; set; }
        public string BookName { get; set; }
        public string OldPrice { get; set; }
        public string NewPrice { get; set; }
        public string Sale { get; set; }
        public string Images { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Author { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }    
    }
}
