﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebSachCu.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int OrderId { get; set; }
        public int? CustomerId { get; set; }
        public double TotalMoney { get; set; }
        public string Payment { get; set; }
        public DateTime? DateOrder { get; set; }
        public string Note { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
