﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebSachCu.Models
{
    public partial class Slider
    {
        public int SliderId { get; set; }
        public string Images { get; set; }
        public string Link { get; set; }
    }
}
