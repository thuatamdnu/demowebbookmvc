﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebSachCu.Models
{
    public partial class Web
    {
        public Book book { get; set; }
        public Customer customer { get; set; }
        public Menu menu { get; set; }
        public Slider slider { get; set; }
        public User user { get; set; }
    }
}
