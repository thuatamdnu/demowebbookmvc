﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebSachCu.Models
{
    public partial class Menu
    {
        public int MenuId { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public int? Orders { get; set; }
        public int? ParentId { get; set; }
    }
}
